Pod::Spec.new do |spec|
  spec.name         = "SCLAlertView-Spry"
  spec.version      = "0.8.5"
  spec.summary      = "Beautiful animated Alert View. Written in Swift but ported to Objective-C"
  spec.homepage     = "https://bitbucket.org/sprygroupllc/sclalertview"
  spec.screenshots  = "https://raw.githubusercontent.com/dogo/SCLAlertView/master/ScreenShots/ScreenShot.png", "https://raw.githubusercontent.com/dogo/SCLAlertView/master/ScreenShots/ScreenShot2.png"

  spec.license            = { :type => "MIT", :file => "LICENSE" }
  spec.author             = { "Chayel Heinsen" => "chayel@wearespry.com" }

  spec.platform           = :ios
  spec.ios.deployment_target = '8.0'
  spec.source             = { :git => "https://chayel-heinsen@bitbucket.org/sprygroupllc/sclalertview.git", :tag => spec.version.to_s }
  spec.source_files       = "SCLAlertView/*"
  spec.requires_arc       = true
end
