//
//  SCLTextField.h
//  SCLAlertView
//
//  Created by Chayel Heinsen  on 5/12/15.
//  Copyright (c) 2015 AnyKey Entertainment. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCLTextField : UITextField

@end
