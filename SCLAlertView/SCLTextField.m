//
//  SCLTextField.m
//  SCLAlertView
//
//  Created by Chayel Heinsen  on 5/12/15.
//  Copyright (c) 2015 AnyKey Entertainment. All rights reserved.
//

#import "SCLTextField.h"

@implementation SCLTextField

// placeholder position
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 25, 0);
}

// text position
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 25, 0);
}

@end
